<div align="center">
    <img src="https://gitlab.com/grzgajda/javascript-function-composition/raw/master/logo.png" alt="javascript function composition" />
</div>

<div align="center">
    <strong>Parsing an array of objects from one structure to another to show the difference between different approaches in programming.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-TypeScript-%234F5D95.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/javascript-function-composition.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-100%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
</div>

<br />
<br />