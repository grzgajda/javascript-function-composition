import * as R from 'ramda'
import { Route, PhraseDictionary } from './fixtures'
import { splitCompose } from './utils/splitToParts';

const addParts = (route: Route): IRouteWithPartsCollection => ({
  ...route,
  parts: splitCompose(route.routePath)
})

const groupToRoutes = (route: { p: string, r: string }) => route.p

const translateRoutes = R.pipe(
  R.map(addParts),
  R.chain(a => R.map(p => ({ p: p, r: a.routeName }), a.parts)),
  R.groupBy(groupToRoutes),
  R.toPairs,
  R.map(a => ({ phrase: a[0], routes: a[1].map(a => a.r) }))
)

export default translateRoutes

interface IRouteWithPartsCollection extends Route {
  parts: string[]
}