import { splitArray } from './utils/splitToParts'
import { Route, PhraseDictionary } from './fixtures'

function arrayMethods(input: Route[]): PhraseDictionary[] {
  const a = input.map(route => ({
    ...route,
    parts: splitArray(route.routePath)
  })).reduce((a, b) => {
    b.parts.forEach(part => {
      if (false === part in a) {
        a[part] = []
      }

      a[part].push(b.routeName)
    })

    return a
  }, {} as { [key: string]: string[] })

  return Object.keys(a).map(phrase => ({
    phrase,
    routes: a[phrase]
  }))
}

export default arrayMethods