export interface Route {
  routeName: string
  routePath: string
}

export interface PhraseDictionary {
  phrase: string
  routes: string[]
}

const inputData: Route[] = [
  { routeName: 'homepage', routePath: '/' },
  { routeName: 'products_index', routePath: '/products' },
  { routeName: 'products_show', routePath: '/products/{id}' },
  { routeName: 'shops_index', routePath: '/shops' },
  { routeName: 'shops_show', routePath: '/shops/{id}' },
  { routeName: 'shops_products', routePath: '/shops/{id}/products' }
]

const outputData: PhraseDictionary[] = [
  { phrase: 'products', routes: ['products_index', 'products_show', 'shops_products'] },
  { phrase: 'shops', routes: ['shops_index', 'shops_show', 'shops_products'] }
]

export { inputData, outputData }