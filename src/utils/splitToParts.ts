import * as R from 'ramda'

const splitCompose = R.pipe(
  R.split('/'),
  R.reject<string, 'array'>(R.isEmpty),
  R.reject<string, 'array'>(R.includes('{'))
)

function splitArray(routePath: string): string[] {
  return routePath
    .split('/')
    .filter(part => part.length > 0)
    .filter(part => part.indexOf('{') !== 0)
}

function splitLoop(routePath: string): string[] {
  const validParts = []
  const parts = routePath.split('/')

  for (const part of parts) {
    if (part.length > 0 && part.indexOf('{') !== 0) {
      validParts.push(part)
    }
  }

  return validParts
}

export default splitCompose
export {
  splitCompose,
  splitArray,
  splitLoop
}