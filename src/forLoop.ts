import { splitLoop } from './utils/splitToParts'
import { Route, PhraseDictionary } from './fixtures'

function createEmptyArray(array: object, param: string) {
  if (false === param in array) {
    array[param] = []
  }
}

function forLoop(input: Route[]): PhraseDictionary[] {
  const results: { [key: string]: string[] } = {}

  input.forEach(route => {
    const parts = splitLoop(route.routePath)

    parts.forEach(part => {
      createEmptyArray(results, part)
      results[part].push(route.routeName)
    })
  })

  return Object.keys(results).map(phrase => ({
    phrase,
    routes: results[phrase]
  }))
}

export default forLoop