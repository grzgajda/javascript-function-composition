import arrayMethods from '../src/arrayMethods'
import withCompose from '../src/compose'
import forLoop from '../src/forLoop'
import { inputData, outputData } from '../src/fixtures'

describe('Example of transforming input data to output', () => {
  test('using for loop', () => {
    const result = forLoop(inputData)
    expect(result).toEqual(outputData)
  })

  test('using different array methods', () => {
    const result = arrayMethods(inputData)
    expect(result).toEqual(outputData)
  })

  test('using compose', () => {
    const result = withCompose(inputData)
    expect(result).toEqual(outputData)
  })
})