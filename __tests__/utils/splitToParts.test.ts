import { splitArray, splitCompose, splitLoop } from '../../src/utils/splitToParts'

describe('split url with "compose" function', () => {
  it('should split path to parts', () => {
    const results = splitCompose('/products')
    expect(results).toEqual(['products'])
  })

  it('should remove unique symbols {}', () => {
    const results = splitCompose('/products/{id}/shops')
    expect(results).toEqual(['products', 'shops'])
  })

  it('should return an iterable with accessible array methods', () => {
    const results = splitCompose('/products/{id}/shops')
    expect(results.length).toEqual(2)
  })
})

describe('splut url with "array methods" function', () => {
  it('should split path to parts', () => {
    const results = splitArray('/products')
    expect(results).toEqual(['products'])
  })

  it('should remove unique symbols {}', () => {
    const results = splitArray('/products/{id}/shops')
    expect(results).toEqual(['products', 'shops'])
  })
})

describe('splut url with "for loop" function', () => {
  it('should split path to parts', () => {
    const results = splitLoop('/products')
    expect(results).toEqual(['products'])
  })

  it('should remove unique symbols {}', () => {
    const results = splitLoop('/products/{id}/shops')
    expect(results).toEqual(['products', 'shops'])
  })
})